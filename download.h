#ifndef DOWNLOAD_H
#define DOWNLOAD_H

#include <iostream>
#include <fstream>

void start_download(struct sockaddr_in* server_addr, std::ofstream& output, unsigned int length);

#endif // DOWNLOAD_H

