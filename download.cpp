#include "download.h"

#include <iomanip>
#include <unistd.h>
#include <assert.h>
#include <deque>

#include "socket_op.h"
#include "datagrams.h"

#define WINDOW_SIZE 100

// Based on https://www.ross.click/2011/02/creating-a-progress-bar-in-c-or-any-other-console-app/
static inline void loadbar (unsigned int x, unsigned int n, unsigned int w = 50)
{
    if( (x != n) && (x % (n/100+1) != 0) ) return;

    float ratio  =  x/(float)n;
    int   c      =  ratio * w;
	
    std::cout << "\033[30;42m Progress:" << std::setw(4) << (int)(ratio*100) << "% \033[0m[";
    for( int x=0; x<c; ++x ) 
    {
    	std::cout << "=";
    }
    if( x != n )
    {
    	std::cout << ">";
		for( unsigned int x=c+1; x<w; ++x )
		{
			std::cout << " ";
		}
    }
    
    if( x != n )
    {
    	std::cout << "]\r" << std::flush;
    }
    else
    {
    	std::cout << "] \033[1mDONE\033[0m\n";
    }
}


int parse_msg (u_int8_t * buffer, u_int8_t * &buff)
{	
	std::string type(buffer, buffer+4);
	//std::cout << "TYPE: #" << type << "#\n";

	if(type != "DATA")
	{
		#ifdef DEBUG
			std::cout << "What the hell is this?\n";
		#endif
		return -1;
	}

	int ptr_start = strchr((char*)buffer+5,' ') - ((char*)buffer);
	
	std::string str_start(buffer+5, buffer+ptr_start);
	//std::cout << "START: #" << str_start << "#\n";
	
	int start = std::stoi( str_start );
	
	int ptr_size = strchr((char*)buffer+ptr_start+1,'\n') - ((char*)buffer);
	
	std::string str_size(buffer+ptr_start+1, buffer+ptr_size);
	//std::cout << "SIZE: #" << str_size << "#\n";
	
	int size = std::stoi( str_size );
	
	buff = new u_int8_t[size];
	memcpy(buff, buffer+ptr_size+1, size);
	
	/*#ifdef DEBUG 
		std::cout << "Buff: #";
		for( int j=0; j<size; ++j )
			std::cout << (int)buff[j] << " " ;
		std::cout << "#\n";
	#endif	*/
	
	return start/1000;
}

void start_download (struct sockaddr_in* server_addr, std::ofstream& output, unsigned int length)
{
	int sockfd = Socket (AF_INET, SOCK_DGRAM, 0);
  
	fd_set read_fd;
	
	unsigned int packets = length / 1000;
	unsigned int rest = length % 1000;
	
	if( not (rest == 0) )
	{
		++packets;
	}
	
	#ifdef DEMO
		for( int i =0; i<=100; ++i )
		{
			loadbar(i, 100);
			usleep(20000);
		}
		return;
	#endif
	
	#ifdef DEBUG
		std::cout << "Packets: " << packets << "\n";
		std::cout << "Preparing window: \n";
	#endif
	
	std::deque< datagrams > window;
	
	unsigned int datagram_nr = 0;
	
	if( packets <= WINDOW_SIZE )
	{
		for( ; datagram_nr < packets-1; ++datagram_nr )
			window.emplace_back( datagram_nr );
		
		if( not (rest == 0) )
		{
			window.emplace_back( datagram_nr, rest );
		}
		else
		{
			window.emplace_back( datagram_nr );
		}
		++datagram_nr;
	}
	else
	{
		for( ; datagram_nr < WINDOW_SIZE; ++datagram_nr )
			window.emplace_back( datagram_nr );
	}
	
	
	loadbar(0, packets);
	
	#ifdef DEBUG
		std::cout << "Send first "<< WINDOW_SIZE << " packets: \n";
	#endif
	
	for( unsigned i=0; i<window.size(); ++i )
	{	
		char * msg =  window[i].get_msg();
		Sendto(sockfd, msg, strlen(msg), 0, server_addr);
	}

	bool 				end = false;
	unsigned int 		recv_packets = 0; 
	struct timeval 		wait_time;
	
	struct sockaddr_in	sender;	
	u_int8_t			buffer[IP_MAXPACKET+1];
	
	#ifdef DEBUG
		std::cout << "Begin main send&reciv loop: \n";
	#endif
	
	while( not end ) // main send&reciv loop
	{
		// prepare to wait
		FD_ZERO (&read_fd);
		FD_SET (sockfd, &read_fd);
		
        wait_time = max_time;
       
        while( wait_time.tv_usec != 0 || wait_time.tv_sec != 0 )
		{
			// wait for response
			#ifdef DEBUG 
				std::cout << "Waiting...\n";
			#endif
	
			int rc = Select (sockfd+1, &read_fd, &wait_time);
	
			if( rc == 0 )
			{
				break;
			}
			
			#ifdef DEBUG 
				std::cout << "Reciving...\n";
			#endif
		
			bzero (&buffer, sizeof(buffer));
		
			ssize_t rec_bytes = Recvfrom (sockfd, buffer, 0, sender);
			
			if( rec_bytes == -1 )
			{
				continue;
			}
			
			if( server_addr->sin_addr.s_addr != sender.sin_addr.s_addr || server_addr->sin_port != sender.sin_port )
			{
				continue;
			}
			
			#ifdef DEBUG 
				std::cout << "Recived msg: ";
				for( int j=0; j<20; ++j ) //for( int j=0; j<rec_bytes; ++j )
					std::cout << buffer[j]; //std::cout << (int)buffer[j] << ' ';
				std::cout << '\n';
			#endif	
			
			// parse response
			// if( good )
			//     add buff to property element in window
			u_int8_t* 	buff;
			int 		nr = parse_msg(buffer, buff);
			
			nr -= recv_packets;
			
			if( nr < 0 )
			{
				continue;
			}
			//std::cout << "!NR: " << nr << "\n";
			
			if( not window[nr].recv )
			{
				#ifdef DEBUG
					std::cout << "!NR: " << nr << "\n";
				#endif

				assert( window[nr].buff == NULL );
				assert( buff != NULL );
				assert( window[nr].number == nr + recv_packets);
				
				window[nr].buff = buff;
				window[nr].recv = true;
				
				//std::cout << "DEBUG: #" << window[nr] << "#";
			}
		
			while( window.size() && window[0].recv )
			{
			
				#ifdef DEBUG
					std::cout << "NR: " << window[0].number << "\n";
				#endif
				output << window[0];
				window.pop_front();
				loadbar( ++recv_packets , packets );
				
				if( datagram_nr < packets-1 )
				{
					window.emplace_back( datagram_nr++ );
				}
				else if( datagram_nr == packets-1 )
				{
					if( not (rest == 0) )
					{
						window.emplace_back( datagram_nr++, rest );
					}
					else
					{
						window.emplace_back( datagram_nr++ );
					}
				}
				else
				{
					continue;
				}
				
				char * msg =  window.back().get_msg();
				Sendto(sockfd, msg, strlen(msg), 0, server_addr);
			}
		
		}
		
		if( recv_packets == packets )
		{
			end = true;
			continue;
		}
		
		for( unsigned i=0; i<window.size(); ++i )
		{	
			//std::cout << "Debug: " << window[i].timeout() << "\n";
			if( not window[i].recv && window[i].timeout() )
			{
				#ifdef DEBUG
					std::cout << "DEBUG: " << window[i].number << " " << window[i].start << " " << window[i].size << "\n";
					std::cout << "get_msg: " << window[i].get_msg() << "\n";
				#endif
				
				char * msg =  window[i].get_msg();
				Sendto (sockfd, msg, strlen(msg), 0, server_addr);
			}
		}
	}
	
	close (sockfd);

}

