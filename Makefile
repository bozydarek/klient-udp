# Autor: Piotr Szymajda - 273 023
CXX = g++
FLAGS = -std=c++11 -Wall -Wextra -pedantic -pedantic-errors# -O2
NAME = client-udp
OBJS = main.o socket_op.o download.o

all: $(OBJS) 
	$(CXX) $^ -o $(NAME)

$(OBJS): %.o: %.cpp
	$(CXX) -c $(FLAGS) $< -o $@

debug: FLAGS += -DDEBUG -g3
debug: all

demo: FLAGS += -DDEMO -O2
demo: all

fast: FLAGS += -O2
fast: all

clean:
	rm -f *.o
	
distclean:
	rm -f $(NAME)
	
realclean: clean distclean  
	
.PHONY: all clean distclean realclean debug
