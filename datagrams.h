#ifndef DATAGRAMS_H
#define DATAGRAMS_H

#include <sstream>
#include <sys/time.h>

#ifdef DEBUG
	#include <iostream>
#endif

const struct timeval max_time = {0,5000};

struct datagrams
{
	bool 			recv;
	u_int8_t* 		buff;
	unsigned int 	number;
	short 			size;
	
	long long int	start;
	struct timeval	time;
	
	datagrams( unsigned int nr, short len = 1000 )
		:recv (false)
		,buff (NULL)
		,number (nr)
		,size (len)
		,start (number*1000)
		{		
            gettimeofday(&time, NULL);
		}
	
	~datagrams()
	{
		assert( recv ); // this shouldn't happen
		
		if( buff != NULL )
			delete[] buff;
	}
	
	char * get_msg()
	{
//	 "GET start size\n"
		char* msg = new char[30]; 
		{
			std::stringstream ss;
			ss << "GET " << start << ' ' << size << '\n';
		  	const std::string& tmp = ss.str();   
		  	strcpy (msg, tmp.c_str());
		}
		return msg;
	}
	
	bool timeout()
	{
		struct timeval now, res;
        gettimeofday(&now, NULL);
        timersub( &now, &time, &res);
                
		#ifdef DEBUG
			std::cout << res.tv_sec << ":" <<res.tv_usec << " > ";
			std::cout << max_time.tv_sec << ":" <<max_time.tv_usec << "\n";
		#endif        

        return timercmp(&res, &max_time, >);
	}
};


std::ostream& 
operator << (std::ostream & os, const datagrams & data)
{
	assert( data.buff != NULL );
	
	for( int i=0; i<data.size; ++i )
	{
		os << data.buff[i];
	}
	return os;

}


#endif
