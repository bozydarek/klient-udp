// Autor: Piotr Szymajda - 273 023
#include "socket_op.h"

#include <errno.h>

void print_error (std::string fname, int exit_no)
{
    std::cout << fname << "(...) failed. \033[1;31mError:\033[0m " << errno << " - "<< strerror (errno) << "\n";
    exit (exit_no); 
}

int Socket (int family, int type, int protocol)
{
	int	fd = socket (family, type, protocol);
	if( fd == -1 )
        print_error ("socket", -1);
    
	return fd;
}

int Sendto (int fd, const void *buff, int buff_length, int flags, const struct sockaddr_in *saddr)
{
	int sent_bytes = sendto (fd, buff, buff_length, flags, (struct sockaddr*)saddr, sizeof(*saddr));
	
	if( sent_bytes == -1 || sent_bytes != buff_length )
	{
		if( errno != ENETUNREACH )
			print_error ("sendto", -2);
		else
			return 1;
	}
	return 0;
}

ssize_t Recvfrom (int fd, void *ptr, int flags, struct sockaddr_in &saddr)
{   
	socklen_t saddr_len = sizeof(saddr);
	ssize_t	rec_bytes = recvfrom (fd, ptr, IP_MAXPACKET, flags, (struct sockaddr*)&saddr, &saddr_len);
	if( rec_bytes == -1 )
		if( errno != EAGAIN )
            print_error ("recvfrom", -4);
			
	return rec_bytes;
}

int Select (int nfds, fd_set *readfds, struct timeval *timeout)
{
	int rc = select (nfds, readfds, NULL, NULL, timeout); // w pierwszym argumencie najwyższy (numer) deskryptor+1
    if( rc < 0 )
        print_error ("select", -5);
        
    return rc;
}


void Getaddrinfo (const char *node, const struct addrinfo *hints, struct addrinfo **res)
{
	int error = getaddrinfo(node, NULL, hints, res);
	if( error != 0 ) 
	{
		if( error == EAI_SYSTEM )
        	print_error ("getaddrinfo", -7);
        else
        {
        	std::cout << "getaddrinfo(...) failed. \033[1;31mError:\033[0m " << error << " - " << gai_strerror (error) << "\n";
        	exit (-8);
        }
	}
}

