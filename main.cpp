/*
Autor: Piotr Szymajda - 273 023

Klient UDP - ściągający z serwera plik z uwzgelędnieniem zawodności komunikacji
*/
#include <iostream>
#include <cstring>
#include <assert.h>
#include <sstream>

#include "socket_op.h"
#include "download.h"

using namespace std;

string serv_addr = "aisd.ii.uni.wroc.pl";

void man ();

int main (int argc, char *argv[])
{
    if (argc != 4)
    {   
        if( argc == 2 && (strcmp (argv [1], "--help") == 0 || strcmp (argv [1], "-h") == 0 ) )
        {
            man ();
        }
        else
        {
            cout << "\033[1;31mError:\033[0m Incorrect use\n";
            man ();
        }
        return 1;
    }
	
	unsigned int port;
	{
	
		istringstream ss( argv[1] );
		if( not (ss >> port) )
		{
			cout << "\033[1;31mError:\033[0m " << argv[1] << " is not a number\n";
			return 1;
		}
		if( not (port > 0) )
		{
			cout << "\033[1;31mError:\033[0m Port must be greater then 0 \n";
			return 2;
		}
	}

	
	struct addrinfo* result;
	struct addrinfo hints = {0, AF_INET, SOCK_STREAM, 0, 0, NULL, NULL, NULL}; 
	
	Getaddrinfo (serv_addr.c_str(), &hints, &result);

	struct sockaddr_in* server_addr;

	bool different = false;
	for( struct addrinfo* r = result; r != NULL; r = r->ai_next ) 
	{ 
		if( different )
		{
			cout << "\033[1;35mWarning:\033[0m Multiple addresses in DNS resolv.\n";
		}
		
		server_addr = (struct sockaddr_in*)(r->ai_addr);
		
		different = true;
		
		#ifdef DEBUG
			char ip_address[20];
			inet_ntop (AF_INET, &(server_addr->sin_addr), ip_address, sizeof(ip_address));
			printf ("%s\n", ip_address);
		#endif
	}
	
	assert( server_addr->sin_family == AF_INET );
	server_addr->sin_port = htons (port);
	
	ofstream output_file;
	output_file.open (argv[2], ios::out | ios::binary);

 	if( not output_file.is_open() )
 	{
		cout << "\033[1;31mError:\033[0m Opening file\n";
		return 5;
	}
	
	unsigned int len;
	{
		istringstream ss( argv[3] );
	
		if( not (ss >> len) )
		{
			cout << "\033[1;31mError:\033[0m " << argv[3] << " is not a number\n";
			return 3;
		}
		if( not (len > 0) )
		{
			cout << "\033[1;31mError:\033[0m Length must be greater then 0 \n";
			return 4;
		}
		if( len > 1000000 )
		{
			cout << "\033[1;31mError:\033[0m Length must be lower or equal to 1'000'000 \n";
			return 4;
		}
		
	}
	
	cout << "Start downloading \033[1m" << len << "\033[0m bytes to the file \033[1m" << argv[2] << "\033[0m from \033[1m"<< serv_addr <<"\033[0m\n";

	start_download (server_addr, output_file, len);

	output_file.close ();
	freeaddrinfo (result);
	
    return 0;
}

/*
Function displays manual
*/
void man()
{
    cout << "\033[1;36mClient UDP\033[0m - program that connects to the serwer\n";
    cout << "\t and download data from it.\n";
    cout << "\n\033[1mUsage:\033[0m\n";
    cout << "\t./client-udp port data_name numer file_size - for start program\n";
    cout << "\t./client-udp -h - for display this help\n";
    cout << "\n\033[1mFor example:\033[0m\n";
    cout << "\t./client-udp 40001 output 1000\n";
}
