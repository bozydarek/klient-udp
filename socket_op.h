// Autor: Piotr Szymajda - 273 023
#ifndef SOCKET_OP_H
#define SOCKET_OP_H

#include <arpa/inet.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>

#include <string.h>
#include <iostream>

int 	Socket (int family, int type, int protocol);
int 	Sendto (int fd, const void *buff, int buff_length, int flags, const struct sockaddr_in *saddr);
ssize_t Recvfrom (int fd, void *ptr, int flags, struct sockaddr_in &saddr);
int 	Select (int nfds, fd_set *readfds, struct timeval *timeout);
void	Getaddrinfo (const char *node, const struct addrinfo *hints, struct addrinfo **res);

void 	print_error (std::string fname, int exit_no);

#endif // SOCKET_OP_H

